package ee.itcollege.mimangus.graphicslib;

import java.awt.Graphics2D;
import java.awt.geom.Area;

/**
 * Interface for a drawable and collidable object
 *
 * @author Mikk Mangus
 */
public interface IDrawable {

	/**
	 * Area for collisions
	 */
	public Area getArea();

	public void drawItself(Graphics2D g);

}