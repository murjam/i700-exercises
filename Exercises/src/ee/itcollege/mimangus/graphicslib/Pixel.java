package ee.itcollege.mimangus.graphicslib;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Area;
import java.awt.geom.Point2D;

/**
 * A simple building block with static position,
 * dynamic position and a color.
 */
public class Pixel {
	
	public static final int PIXEL_SIZE = 5;

	public Point2D.Double position;
    Color color;
    
    public Pixel(int x, int y) {
    	this(x, y, Color.white);
    }
    
    public Pixel(int x, int y, Color color) {
        position = new Point2D.Double(x, y);
        this.color = color;
    }
    
	public void drawItself(Point2D.Double offset, Graphics2D g) {
        g.setColor(color);
	    g.fill(getArea(offset));
    }

	public Area getArea(Point2D.Double offset) {
		return new Area(new Rectangle(
				(int)(offset.x * PIXEL_SIZE + position.x * PIXEL_SIZE),
				(int)(offset.y * PIXEL_SIZE + position.y * PIXEL_SIZE),
				PIXEL_SIZE,
				PIXEL_SIZE));
	}

}



