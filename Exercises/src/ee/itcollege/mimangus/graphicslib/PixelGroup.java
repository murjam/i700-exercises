package ee.itcollege.mimangus.graphicslib;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * A class for any drawable and collidable object
 * consisting of blocks (Pixels) and having a position.
 */
public class PixelGroup implements IDrawable {
	
	public Point2D.Double position = new Point2D.Double(0, 0);	
	public ArrayList<Pixel> blocks = new ArrayList<Pixel>();
	
	/**
	 * Creates a new PixelGroup object
	 * 
	 * @param coordinates
	 * 			Two-dimensional integer array of Coordinates of pixels.
	 * 			Zero stands for no pixel, any other value stands for
	 * 			a pixel of a color.
	 * @param colors
	 * 			Array of Colors used to create pixels. Pixel with value 1
	 * 			has the first color from this array, 2 has the second color etc.
	 */
	public PixelGroup(int[][] coordinates, Color[] colors) {
		for (int i = 0; i < coordinates.length; i++) {
			for (int j = 0; j < coordinates[i].length; j++) {
				if (0 != coordinates[i][j]) {
					blocks.add(new Pixel(j, i, colors[coordinates[i][j] - 1]));
				}
			}
		}
	}

	@Override
	public void drawItself(Graphics2D g) {
		for (Pixel part : blocks) {
			part.drawItself(getPosition(), g);
		}
	}
	
	@Override
	public Area getArea() {
		Area area = new Area();
		for (Pixel part : blocks) {
			area.add(part.getArea(getPosition()));
		}
		return area;
	}
	
	public Point2D.Double getPosition() {
		return position;
	}
	
}
