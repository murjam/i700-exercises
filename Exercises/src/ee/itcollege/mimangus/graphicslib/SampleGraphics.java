package ee.itcollege.mimangus.graphicslib;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class SampleGraphics extends JPanel {

	SampleOfPixelGroup spaceShip = new SampleOfPixelGroup();
	
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.black);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		Graphics2D g2 = (Graphics2D) g;
		
		spaceShip.drawItself(g2);
	}
}
