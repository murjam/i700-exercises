package ee.itcollege.mimangus.graphicslib;

import java.awt.Color;


public class SampleOfPixelGroup extends PixelGroup {
	
	public SampleOfPixelGroup() {
		super(new int[][]{
			{0, 1, 0, 0, 0, 1, 0},
			{0, 1, 1, 1, 1, 1, 0},
			{1, 1, 2, 1, 2, 1, 1},
			{1, 1, 1, 1, 1, 1, 1},
			{1, 0, 1, 0, 1, 0, 1},
		}, new Color[] {
			Color.WHITE,
			Color.RED,
		});
	}

}
