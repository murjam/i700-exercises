package ee.itcollege.mimangus.graphicslib;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class SampleProgramStart implements Runnable {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new SampleProgramStart());
	}
	
	@Override
	public void run() {
		
		JFrame window = new JFrame("A sample of an game");
		
		window.setSize(500, 400);
		window.setContentPane(new SampleGraphics());
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
		
	}

}
