package week10.practicum;

import java.util.Arrays;

public class Matrix {
	
	public static int max(int[] row) {
		if (row.length < 1) {
			return 0;
		}
		Arrays.sort(row);
		return row[row.length - 1];
	}
	
	public static int sumOfRowMax(int[][] matrix) {
		int sum = 0;
		
		for (int[] row : matrix) {
			sum = sum + max(row);
		}
		
		return sum;
	}

	public static void main(String[] args) {
		
		System.out.println(max(new int[] {1, 2, 4, 5, 2, 8, 2}));
		
		 System.out.println(sumOfRowMax(new int[][] {
		    {4, 6, 1, 9},   // max: 9
		    {-5, -10, -12}, // max: -5
		    {0, 2, 1},      // max: 2
		    {}
		 }));
	}
	
}
