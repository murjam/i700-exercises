package week10.practicum;

import java.io.File;

public class MyFiles {
	
	public static void listFiles(File folder) {
		try {
			for (File file : folder.listFiles()) {
				if (file.getName().endsWith(".java")) {
					System.out.println(file);
				}
				if (file.isDirectory()) {
					listFiles(file);
				}
			}
		}
		catch (NullPointerException e) {
			// just ignore
		}
	}
	
	public static void main(String[] args) {
		listFiles(new File("/"));
	}

}
