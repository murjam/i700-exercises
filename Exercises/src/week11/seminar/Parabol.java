package week11.seminar;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Parabol implements Runnable {
	
	public static final int GRAPHICS_WIDTH = 500;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Parabol());
	}
	
	@Override
	public void run() {
		JFrame window = new JFrame();
		window.setContentPane(new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				g.setColor(Color.gray);
				
				g.drawLine(0, GRAPHICS_WIDTH / 2,  GRAPHICS_WIDTH, GRAPHICS_WIDTH / 2);
				g.drawLine(GRAPHICS_WIDTH / 2, 0,  GRAPHICS_WIDTH / 2, GRAPHICS_WIDTH);
				
				g.setColor(Color.black);
				double a = .05, b = -5, c = 4;
				for (double x = -GRAPHICS_WIDTH / 2; x < GRAPHICS_WIDTH / 2; x++) {
					double y = a * x * x + b * x + c;
					g.fillRect((int)x + GRAPHICS_WIDTH / 2, (int)-y + GRAPHICS_WIDTH / 2, 3, 3);
				}				
			}
		});
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setSize(GRAPHICS_WIDTH, GRAPHICS_WIDTH);
		window.setVisible(true);
	}
	
}
