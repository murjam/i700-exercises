package week2.practicum;

import lib.TextIO;

public class GroupSize {

	public static void main(String[] args) {
		System.out.println("Insert the number of people");
		int people = TextIO.getlnInt();
		System.out.println("Insert the group size");
		int size = TextIO.getlnInt();
		
		int groups = people / size;
		int leftover = people % size;
		
		System.out.format("You get %d full groups, leftover %d.",
				groups, leftover);
	}
	
}
