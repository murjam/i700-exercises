package week2.practicum;

import lib.TextIO;

public class NameLength {

	public static void main(String[] args) {
		
		int number = 3;
		
		String text = "Some text for sure";
		
		System.out.println(text.charAt(1));
		System.out.println(text.toUpperCase());
		System.out.println(text.contains("sure"));
		String[] split = text.split(" ");
		
		
		// regular expression
		System.out.println(text.replaceAll("[sS]", "_"));
		
		
		System.out.println("Write your name");
		String name = TextIO.getlnString();
		int nameLength = name.length();
		
		System.out.format("Your name has %d characters.", nameLength);
	}
	
}
