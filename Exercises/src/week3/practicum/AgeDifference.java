package week3.practicum;

import lib.TextIO;

public class AgeDifference {

	public static void main(String[] args) {
		
		int positive;
		while (true) {
			System.out.println("Insert a positive number");
			positive = TextIO.getlnInt();
			if (positive > 0) {
				break;
			}
		}
		
		System.out.println("The number is positive: " + positive);
		
		
		
		while (true) {
			System.out.println("Insert the age of yourself and your girlfriend.");
			
			int age1 = TextIO.getlnInt();
			int age2 = TextIO.getlnInt();
			
			int diff = Math.abs(age1 - age2);
		
			String opinion;		
			if (diff < 5) {
				opinion = "Okay";
			}
			else if (diff <= 10) { // 5..10
				opinion = "Very nice";
			}
			else if (diff <= 15) {
				opinion = "That's strange";
			}
			else {
				opinion = "Not good at all";
			}
			
			System.out.println("People think: " + opinion + "!");
			
			System.out.format("Peple think: %s!\n", opinion);
		}
		
		
		
	}

}
