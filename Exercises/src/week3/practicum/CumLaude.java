package week3.practicum;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {
		
		System.out.println("Insert the average grade");
		
		double average = TextIO.getlnDouble();
		
		System.out.println("Insert the average of diploma thesis");
		
		int thesis = TextIO.getlnInt();
		
		if (thesis == 5 && average > 4.5) {
			System.out.println("Yeah you get Cum Laude!");				
		}
		else {
			System.out.println("Sorry no Cum laude for you!");
		}
		
	}

}
