package week3.practicum;

import lib.TextIO;

public class OddOrEven {
	
	public static void main(String[] args) {
		System.out.println("Insert a number");
		
		int number = TextIO.getlnInt();
		
		if (0 == number % 2) {
			System.out.println("The number is odd.");
		}
		else {
			System.out.println("The number is even.");
		}
		
	}

}
