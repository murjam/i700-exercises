package week3.practicum;

import lib.TextIO;

public class Password {

	public static void main(String[] args) {

		System.out.println("Type a password");
		
		String password = TextIO.getlnString();
		
		if ("kalakala".equals(password)) {
			System.out.println("password is right");
		}
		else {
			System.out.println("password is wrong");
		}
		
	}
}
