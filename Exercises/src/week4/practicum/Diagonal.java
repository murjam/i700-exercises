package week4.practicum;

import lib.TextIO;

public class Diagonal {

	public static void main(String[] args) {
		int size;
		
		while (true) {
			System.out.println("Insert a positive odd number");
			size = TextIO.getlnInt();
			
			if (size > 0) {
				if (size % 2 == 1) {
					break;
				}
				else {
					System.out.println("It has to be odd!");
				}
			}
			else {
				System.out.println("It has to be positive!");
			}
		}

		int lineSize = size * 2 + 3;
		//String line = String.format("%" + lineSize + "s", "").replace(' ', '-');
		String line = "";
		for (int i = 0; i < lineSize; i++) {
			line = line + "-";
		}
		
		
		System.out.println(line);	
		
		for (int row = 0; row < size; row++) {
			System.out.print("| ");
			
			for (int col = 0; col < size; col++) {
				// short if, inline if				
				System.out.print(col == row || col + row == size - 1 ? "X " : ". ");
			}
			System.out.println("|");
			
		}
		
		System.out.println(line);
		
		
		
	}
	
}
