package week4.practicum;

public class NumberLines {

	public static void main(String[] args) {

		for (int i = 10; i >= 1; i--) {
			System.out.print(i + " ");
		}
		System.out.println();
		
		for (int i = 0; i < 10; i++) {
			System.out.print((10 - i) + " ");
		}
		System.out.println();
		
		for (int i = 0; i <= 10; i += 2) {
			System.out.print(i + " ");
		}
		System.out.println();
		
		for (int i = 0; i < 30; i++) {
			int number = 30 - i; 
			if (number % 3 == 0) {
				System.out.print(number + " ");
			}
		}
		System.out.println();
		
	}

}
