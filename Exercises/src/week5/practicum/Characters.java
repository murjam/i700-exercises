package week5.practicum;

public class Characters {

	/**
	 * Gives you a text containing of x times of character c.
	 * @param c the character to repeat
	 * @param x how many times you want the character
	 */
	public static String xCharacters(char c, int x) {
		String chars = "";
		
		for (int i = 0; i < x; i++) {
			chars = chars + c;
		}
		
		return chars;
	}

	public static void main(String[] args) {
		int size = 10;
		
		for (int i = 0; i < size; i++) {
			System.out.format("%s%s%" + size + "s\n",
					xCharacters('-', size - i),
					xCharacters(' ', i),
					xCharacters('-', size - i));
		}
		
		for (int i = 0; i < size; i++) {
			System.out.format("%s%s%" + size + "s\n",
					xCharacters('-', i),
					xCharacters(' ', size - i),
					xCharacters('-', i));
		}
		
	}
	
}
