package week5.practicum;

public class Random {
	
	
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			System.out.println(random(100, -200));
		}
	}

	public static int random(int min, int max) {
		
		int variations;
		
		if (min < max) {
			variations = max - min + 1;
		}
		else {
			variations = min - max + 1;
			min = max;
		}
		
		return (int)(Math.random() * variations) + min;
	}

}
