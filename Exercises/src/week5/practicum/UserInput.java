package week5.practicum;

import lib.TextIO;

public class UserInput {

	public static int getNumber(String question, int min, int max) {
		while (true) {
			System.out.println(question);
			int number = TextIO.getlnInt();
			
			if (min <= number && number <= max) {
				return number;
			}
		}
	}
	
	public static int getNumber(int min, int max) {
		String question = String.format("Please insert a number between %d and %d.", min, max);
		return getNumber(question, min, max);
	}
	
	public static void main(String[] args) {
		
		int number = getNumber(1, 10);
		System.out.println("The number inserted: " + number);
		
		int coin = getNumber("Heads (1) or tails (0)?", 0, 1);
		System.out.println(coin);
		
	}
	
}
