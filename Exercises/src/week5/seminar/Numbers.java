package week5.seminar;

public class Numbers {


	// should never exceed 100 or -100
	public static int sum(int a, int b) {
		return sum(a, b, 0);
	}
	
	/**
	 * Sums up the parameters, but returns nothing greater
	 * than 100 or less than -100
	 * 
	 * @return The sum of parameters.
	 * 	If the sum is greater than 100, it will return 100.
	 *  If the sum is less than -100, it will return -100. 
	 */
	public static int sum(int a, int b, int c) {
		long sum = (long) a + (long) b + (long) c;
		
		if (sum > 100) {
			return 100;
		}
		else if (sum < -100) {
			return -100;
		}
		else {
			return (int) sum;
		}
	}
	
}
