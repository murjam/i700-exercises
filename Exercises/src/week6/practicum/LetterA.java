package week6.practicum;

import java.util.ArrayList;

import lib.TextIO;

public class LetterA {
	
	public static int countAs(String name) {
		
		// return name.length() - name.toLowerCase().replace("a", "").length();
		
		int count = 0;
		name = name.toLowerCase();
		
		for (int i = 0; i < name.length(); i++) {
			if (name.charAt(i) == 'a') {
				count++;
			}
		}
		
		return count;
	}

	public static void main(String[] args) {
		
		ArrayList<String> names = new ArrayList<String>();
		
		while (true) {
			System.out.format("Insert %s: ", names.isEmpty() ? "a name" : "another");
			String input = TextIO.getlnString().trim();
			
			if (input.isEmpty()) {
				if (names.isEmpty()) {
					System.out.println("Commoon, insert at least one name");
					continue;
				}
				else {
					break;
				}
			}
			
			names.add(input);
		}
		
		
		System.out.println();
		for (String name : names) {
			System.out.format("%3d %s\n", countAs(name), name);
		}
		
	}
}
