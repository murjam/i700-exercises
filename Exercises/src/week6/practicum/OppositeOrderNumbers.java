package week6.practicum;

import java.util.ArrayList;
import java.util.Collections;

import lib.TextIO;

public class OppositeOrderNumbers {

	public static void main(String[] args) {
		int count = 5;
		
		
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		
		System.out.format("Please insert %d numbers:\n", count);
		for (int i = 0; i < count; i++) {
			numbers.add(TextIO.getlnInt());
		}
		
		// numbers.length - 1 .. 0
		for (int i = 0; i < numbers.size(); i++) {
			int oppositeIndex = numbers.size() - i - 1;
			System.out.print(" " + numbers.get(oppositeIndex));
		}
		System.out.println();
		
		for (int i = numbers.size() - 1; i >= 0; i--) {
			System.out.print(" " + numbers.get(i));
		}
		System.out.println();
		
		Collections.reverse(numbers);
		for (Integer number : numbers) {
			System.out.print(" " + number);
		}

	}
	
}
