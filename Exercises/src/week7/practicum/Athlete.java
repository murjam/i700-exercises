package week7.practicum;

public class Athlete extends Human {
	
	private Double result;
	
	public Athlete(String name, double result) {
		super(name);
		this.setResult(result);
	}
	
	@Override
	public String toString() {
		return String.format("Athlete %s, has a record of %.2f", getName(), getResult());
	}

	public Double getResult() {
		return result;
	}

	public void setResult(double result) {
		this.result = result;
	}
}