package week7.practicum;

public class Human {

	private String name;
	public int age;
	
	public Human(String name) {
		this.setName(name);
	}
	
	public void greet() {
		if (age > 3) {
			System.out.format("Hello, I am %s, and I'm %d years old.\n", getName(), age);
		} else {
			System.out.println("Boo boo");
		}
	}
	
	@Override
	public String toString() {
		return String.format("%s, %d years old", getName(), age);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}