package week7.practicum;

import java.util.ArrayList;

import lib.TextIO;

	public class Persons {
	
		public static void main(String[] args) {
			
			ArrayList<Human> persons = new ArrayList<>();
			
			while (true) {
				System.out.println("Insert a name");
				String name = TextIO.getlnString().trim();
				
				if (name.isEmpty()) {
					break;			
				}
				persons.add(new Human(name));
			}
				
			for (Human human : persons) {
				System.out.println("Insert the age of " + human.getName());
				human.age = TextIO.getlnInt();
			}
				
			// debugging
			System.out.println(persons);
			System.out.println();
			
			for (Human person : persons) {
				person.greet();
			}
		}
	}
	
