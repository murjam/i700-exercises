package week7.practicum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import lib.TextIO;

public class SportsEvent {

	public static void main(String[] args) {
		
		ArrayList<Athlete> athletes = new ArrayList<>();
		
		while (true) {
			System.out.println("Insert the name and result on one line");
			String input = TextIO.getlnString().trim();
			
			if (input.isEmpty()) {
				break;
			}
			
			String[] words = input.split(" ");
			
			try {
				String name = "";
				for (int i = 0; i < words.length - 1; i++) {
					name += words[i] + " ";
				}
				name = name.trim();
				
				double result = Double.parseDouble(words[words.length - 1]);
				
				Athlete athlete = new Athlete(name, result);
				athletes.add(athlete);
				System.out.println("Athlete " + athlete.getName() + " was added.");
			}
			catch (RuntimeException e) {
				System.out.println("Problem with the input, no athlete was added.");
				System.out.println(e);
			}
			
		}
		
		Collections.sort(athletes, new Comparator<Athlete>() {
			@Override
			public int compare(Athlete a1, Athlete a2) {
				return -a1.getResult().compareTo(a2.getResult());
			}
		});
		
		for (Athlete athlete : athletes) {
			System.out.format("%5.1f %s\n", athlete.getResult(), athlete.getName());
		}
		
	}
	
}
