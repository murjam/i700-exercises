package week7.seminar;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Headlines {
	
	public static String getFirstHeadline(String url, String cssSelector) throws IOException {
		Document document = Jsoup.connect(url).get();
		
		Elements links = document.select("a");
		for (Element link : links) {
			System.out.println(link.absUrl("href"));
		}
		
		
		Elements elements = document.select(cssSelector);
		return String.format("%s ::: %s", url, elements.first().text());
	}

	public static void main(String[] args) throws IOException {
		
		System.out.println(getFirstHeadline("http://www.thehackernews.com", "h2.post-title"));
		System.out.println(getFirstHeadline("http://www.postimees.ee", "h1.frontHeading"));
		System.out.println(getFirstHeadline("http://www.err.ee", "h3"));
	}

}
