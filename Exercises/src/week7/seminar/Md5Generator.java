package week7.seminar;

import org.apache.commons.codec.digest.DigestUtils;

import lib.TextIO;

public class Md5Generator {

	public static void main(String[] args) {
		
		System.out.println("Input something: ");
		String input = TextIO.getlnString();
		System.out.println(DigestUtils.md5Hex(input));		
	}
}
