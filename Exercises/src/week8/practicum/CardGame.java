package week8.practicum;

import java.util.ArrayList;
import java.util.Collections;

public class CardGame {
	
	public static ArrayList<Card> newSetOfCards() {
		ArrayList<Card> set = new ArrayList<>();
		
		for (Suit suit : Suit.values()) {
			for (Rank rank : Rank.values()) {
				set.add(new Card(rank, suit));
			}
		}
		
		return set;		
	}

	public static void main(String[] args) {
		ArrayList<Card> set = newSetOfCards();
		
		Collections.shuffle(set);
		
		ArrayList<Card> hand = new ArrayList<>();
		
		int index = 0;
		while (hand.size() < 5) {
			hand.add(set.remove(index++));
		}
		
		for (Card card : hand) {
			System.out.println(card);
		}
		
	}
	
}
