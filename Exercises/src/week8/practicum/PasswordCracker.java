package week8.practicum;

import org.apache.commons.codec.digest.DigestUtils;

public class PasswordCracker {
	
	public static final String LOOKUP_HASH
		= "19dd1947a95454ccaf223a731c32db0c";
	
	public static final String POSSIBLE
		= "abcdefghijklmnopqrstuvwxyz0123456789";
	
	public static boolean hasTheRightHash(String text) {
		return DigestUtils.md5Hex(text).equals(LOOKUP_HASH);
	}
	

	public static void main(String[] args) {
			
		int length = POSSIBLE.length();

		for (int i = 0; i < length; i++) {
			for (int j = 0; j < length; j++) {
				for (int k = 0; k < length; k++) {
					for (int l = 0; l < length; l++) {
						String word = String.valueOf(POSSIBLE.charAt(i))
							+ POSSIBLE.charAt(j)
							+ POSSIBLE.charAt(k)
							+ POSSIBLE.charAt(l);
						if (hasTheRightHash(word)) {
							System.out.println("Found the word! " + word);
							return;
						}
					}
				}
			}
		}
		
		
	}

}
