package week9.practicum;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class FileManager {

	public static File findFile(Class<?> clazz, String fileName) {
		return new File(clazz.getResource("").toString().substring(5) + fileName);
	}
	
	
	public static ArrayList<String> readFile(Class<?> clazz, String fileName) throws FileNotFoundException {
		return readFile(findFile(clazz, fileName));
	}
	
	public static ArrayList<String> readFile(File file) throws FileNotFoundException {
		ArrayList<String> contents = new ArrayList<String>();
		try {
						
			BufferedReader reader =
					new BufferedReader(new FileReader(file));
			
			String line;
			while (null != (line = reader.readLine())) {
				contents.add(line);
			}
			
			reader.close();
			
		}
		catch (FileNotFoundException e) {
			throw e;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return contents;
		
	}
	
	public static void writeToFile(Class<?> clazz, String fileName, ArrayList<String> contents) {
		writeToFile(findFile(clazz, fileName), contents);
	}
	
	public static void writeToFile(File file, ArrayList<String> contents) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			for (String line : contents) {
				writer.write(line + "\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
