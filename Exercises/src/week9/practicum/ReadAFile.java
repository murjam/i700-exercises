package week9.practicum;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;

public class ReadAFile {

	public static void main(String[] args) throws FileNotFoundException, URISyntaxException {
		
		System.out.println(FileManager.readFile(Practicum9Context.class, "test.txt"));
		
	}
	
}
