package week9.practicum;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import lib.TextIO;

public class TodoList {
	static ArrayList<String> todo = new ArrayList<String>();
	
	public static void writeTodoToFile() {
		FileManager.writeToFile(TodoList.class, "todo.txt", todo);
	}
	
	public static void readTodoFromFile() throws FileNotFoundException {
		todo = FileManager.readFile(TodoList.class, "todo.txt");
	}
	
	public static void printTodo() {
		int num = 0;
		for (int i = 0; i < 100; i++) {
			System.out.println();
		}
		System.out.println("TODO:");
		for (String item : todo) {
			System.out.format("%d %s\n", num++, item);
		}
	}

	public static void main(String[] args) {
		
		try {
			readTodoFromFile();
		}
		catch (FileNotFoundException e) {
			System.out.println("Your todo is empty");
		}
		while (true) {
			printTodo();
			System.out.println();
			System.out.println("Add something to the list:");
			String input = TextIO.getlnString().trim();
			if (input.isEmpty()) {
				return;
			}
			try {
				int index = Integer.parseInt(input);
				if (todo.size() < index + 1) {
					System.out.println("There is no index " + index);
					continue;
				}
				todo.remove(index);
			}
			catch (NumberFormatException e) {
				todo.add(input);
			}
			
			writeTodoToFile();
		}
	}
	
}
