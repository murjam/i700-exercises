package week9.practicum;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class WriteToAFile {

	public static void main(String[] args) throws FileNotFoundException, URISyntaxException {
		
		ArrayList<String> contents = new ArrayList<>();
		contents.add("line1");
		contents.add("line2");
		contents.add("line3");
		
		
		FileManager.writeToFile(Practicum9Context.class, "output.txt", contents);
		
		
	}
	
}
