package week9.seminar;


import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;


public class JavaFxSample extends Application {

	Circle circle = new Circle(20);
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		
		stage.setTitle("Window title");
		Button button = new Button("change the color");
		
		Pane layout = new VBox();
		circle.setFill(Color.RED);
		
		layout.getChildren().add(circle);
		layout.getChildren().add(button);
		
		button.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				circle.setFill(circle.getFill().equals(Color.RED) ? Color.GREEN : Color.RED);
			}
		});
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				circle.setFill(Color.YELLOW);
			}
		}, 5000, 5000);
		
		
		Scene scene = new Scene(layout, 400, 200);
		stage.setScene(scene);
		stage.show();
		
	}

}