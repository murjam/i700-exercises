package week5.practicum;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CharactersTest {

	@Test(timeout=1000)
	public void checkCharacters() {
		
		assertEquals("---", Characters.xCharacters('-', 3));
		assertEquals("aa", Characters.xCharacters('a', 2));
		
		assertEquals("            ",
				Characters.xCharacters(' ', 12));
	}

	@Test
	public void extremeCases() {
		
		assertEquals("", Characters.xCharacters('-', 0));
		assertEquals("", Characters.xCharacters('a', -5));
		
	}
}
