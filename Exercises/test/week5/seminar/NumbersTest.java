package week5.seminar;

import junit.framework.TestCase;

public class NumbersTest extends TestCase {

	public void testSum() {
		
		assertEquals(7, Numbers.sum(3, 4));
		assertEquals(7, Numbers.sum(4, 3));
		
		assertEquals(83, Numbers.sum(55, -5, 33));
		assertEquals(30, Numbers.sum(26, 4));
	}
	
	public void testSumMax() {
		assertEquals(100, Numbers.sum(90, 67, -5));
		assertEquals(100, Numbers.sum(90, 10));
		assertEquals(100, Numbers.sum(1000, -700));
		assertEquals(99, Numbers.sum(55, 22, 22));
		assertEquals(100, Numbers.sum(67, 33));
		assertEquals(100, Numbers.sum(66, 35));
	}
	
	public void testSumMin() {
		assertEquals(-100, Numbers.sum(-67, -34));
		assertEquals(-99, Numbers.sum(-66, -33));
		assertEquals(-100, Numbers.sum(Integer.MIN_VALUE, 100, 5));
	}
	
	public void testSumExtremes() {
		assertEquals(100, Numbers.sum(Integer.MAX_VALUE, 1));
		assertEquals(100, Numbers.sum(943453455, 956645646, 756756756));
		assertEquals(-100, Numbers.sum(-943453455, -956645646, -756756756));
	}
	
}
